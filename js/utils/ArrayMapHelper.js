import dateformat from 'dateformat'

class ArrayMapHelper {
  constructor() { }

  map(products, ads) {
    for (let i = 0; i < products.length; i++) {
      if (i > 0 && (i % 20) === 0 && products[i].type !== 'ad') {
        let p = { id: 'ad' + i, face: 'addd', type: 'ad' };
        products.splice(i, 0, p);
      }
      if (products[i].type !== 'ad') {
        products[i] = this.formatDate(products[i]);
      }
    }
    return products;
  }

  formatDate(product) {
    let date1 = new Date(Date.parse(product.date));
    let date2 = new Date();
    let timeDiff = Math.abs(date2.getTime() - date1.getTime());
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if (diffDays < 7) {
      product.relativeDate = `${diffDays} days ago`;
    }
    else {
      product.relativeDate = dateformat(date1, "dddd mmmm dS, yyyy");
    }
    return product;
  }
}

export default new ArrayMapHelper();