import React from "react";

class ProductItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="offset-by-one three columns product_item" key={this.props.product.id}>
                <a href={this.props.product.url} className="title">{this.props.product.face}</a>
                <br />
                {this.props.product.id}
                <br />
                ${this.props.product.price} - Size: <b>{this.props.product.size}</b>
                <br />
                {this.props.product.relativeDate}
            </div>
        );
    }
}

export default ProductItem;