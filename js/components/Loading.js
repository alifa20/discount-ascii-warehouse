import React from "react";

class Loading extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row" >
                <div className="offset-by-two three columns">
                    <img  src="loading.gif"/>
                </div>
            </div>
        );
    }
}

export default Loading;