import React from "react";
import API from "../API";
import ProductStore from "../stores/ProductStore";
import ProductItem from "./ProductItem";
import LoadMoreLink from "./LoadMoreLink";
import EndOfCatalogue from "./EndOfCatalogue";
import ListFilterButtons from "./ListFilterButtons";
import AdItem from "./AdItem";
import Loading from "./Loading";

let _getAppState = (skip) => {
    return {
        products: ProductStore.getAll(),
        skip: ProductStore.getAllCount(),
        sort: ProductStore.getSort(),
        loadingStatus: ProductStore.getLoadingStatus(),
        isListFinished: ProductStore.getIsListFinished(),
    };
};

export default class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = _getAppState(0);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        API.fetchProducts(0);
        ProductStore.on("change", this.onChange);
    }
    componentWillUnmount() {
        ProductStore.removeListener("change", this.onChange);
    }

    onChange() {
        this.setState(_getAppState(this.state.skip));
    }

    render() {
        let content = this.state.products.map((product, i) => {
            if (product.type === 'ad') {
                return (
                    <AdItem key={product.id}/>
                );
            }
            return (
                <ProductItem product={product} key={product.id}/>
            );
        });
        return (
            <div>
                <h3>Products</h3>
                <ListFilterButtons key="ListFilterButtons" />
                <hr className="offset-by-one ten columns productListFilterDvider"/>
                <div className="row">
                    {content}
                </div>
                
                { this.state.loadingStatus ? <Loading /> :
                    !this.state.isListFinished ?
                        <LoadMoreLink key="LoadMoreLink" skip={this.state.skip} sort={this.state.sort}/>
                        : <EndOfCatalogue />  }
            </div>
        );
    }
}
