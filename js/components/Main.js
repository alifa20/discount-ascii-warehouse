import React from "react";
import ProductList from "./ProductList";

export default class Main extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ProductList key="ProductList"/>
    );
  }
}
