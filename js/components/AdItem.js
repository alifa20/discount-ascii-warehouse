import React from "react";

class AdItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="offset-by-one three columns product_item">
                <img className="ad adItem" src={"/ad/?r=" + Math.floor(Math.random() * 1000) }/>
            </div>
        );
    }
}

export default AdItem;