import React from "react";
import API from "../API";

class LoadMoreLink extends React.Component {
    constructor(props) {
        super(props);
    }
    
    onLoadMorePressed(e) {
        e.preventDefault();
        API.fetchProducts(this.props.skip, this.props.sort);
    }
        
    render() {
        return (
            <div className="row">
                <div className="offset-by-one two columns">
                    <a href='#' className="title" onClick={this.onLoadMorePressed.bind(this)}>Load more ...</a>
                </div>
            </div>
        );
    }
}

export default LoadMoreLink;