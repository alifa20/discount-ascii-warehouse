import React from "react";
import ServerActions from "../actions/ServerActions";
import API from "../API";

class ListFilterButtons extends React.Component {
    constructor(props) {
        super(props);
    }

    onSortPressed(sort) {
        ServerActions.resetProducts();
        API.fetchProducts(0, sort);
    }

    render() {
        return (
            <div className="row">
                <div className="offset-by-one ten columns">
                    <div className="row">
                        <div className="one column">
                            Sort:
                        </div>
                        <div className="two columns">
                            <a className="button button-primary" onClick={this.onSortPressed.bind(this, 'id') }>ID</a>
                        </div>
                        <div className="two columns">
                            <a className="button button-primary" onClick={this.onSortPressed.bind(this, 'size') }>Size</a>
                        </div>
                        <div className="two columns">
                            <a className="button button-primary" onClick={this.onSortPressed.bind(this, 'price') }>Price</a>
                        </div>
                        <div className="six columns">
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ListFilterButtons;
