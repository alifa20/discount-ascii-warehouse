import React from "react";
import API from "../API";

class EndOfCatalogue extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="offset-by-one ten columns">
                    ~ end of catalogue ~
                </div>
            </div>
        );
    }
}

export default EndOfCatalogue;