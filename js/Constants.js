export let ActionTypes = {
  RECEIVE_PRODUCTS: 'RECEIVE_PRODUCTS',
  RESET_PRODUCTS: 'RESET_PRODUCTS',
  LOADING_PRODUCTS: 'LOADING_PRODUCTS',
  BUFFER_TO_PRODUCTS: 'BUFFER_TO_PRODUCTS',
};
