import AppDispatcher from "../AppDispatcher";
import {ActionTypes} from "../Constants";

let ServerActions = {
  loadingProducts(payload) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.LOADING_PRODUCTS,
      payload
    });
  },
  receiveProducts(payload) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.RECEIVE_PRODUCTS,
      payload
    });
  },
  loadBufferToProducts() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.BUFFER_TO_PRODUCTS
    });
  },
  resetProducts() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.RESET_PRODUCTS
    });
  }
};

export default ServerActions;
