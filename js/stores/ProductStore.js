import AppDispatcher from "../AppDispatcher";
import {ActionTypes} from "../Constants";
import {EventEmitter} from "events";
import ArrayMapHelper from '../utils/ArrayMapHelper';

let _products = [];
let _bufferedProducts = [];
let _sort = '';
let _skip = 0;
let _limit = 0;
let _loadingStatus = true;
let _isListFinished = false;

class ProductStore extends EventEmitter {
    constructor(props) {
        super(props);

        AppDispatcher.register(action => {
            switch (action.actionType) {
                case ActionTypes.LOADING_PRODUCTS:
                    _loadingStatus = action.payload.status;
                    this.emit("change");
                    break;
                case ActionTypes.BUFFER_TO_PRODUCTS:
                    console.log('BUFFER_TO_PRODUCTS');
                    _products = _products.concat(_bufferedProducts);
                    _bufferedProducts = [];
                    this.emit("change");
                    break;
                case ActionTypes.RECEIVE_PRODUCTS:
                    _skip = action.payload.skip;
                    _sort = action.payload.sort;
                    _limit = action.payload.limit;


                    if (_limit > 15 && _products.length < 15) {
                        // We are in initial load phase
                        _products.push(action.payload.data);
                    }
                    else if (_limit > 15 && _products.length >= 15) {
                        // We are still in initial load phase
                        _bufferedProducts.push(action.payload.data);
                    }
                    else {
                        // Load more selected
                        _bufferedProducts.push(action.payload.data);
                    }
                    _products = ArrayMapHelper.map(_products);

                    if (_bufferedProducts.length < 1) {
                        _isListFinished = true;
                    } else {
                        _isListFinished = false;
                    }
                    this.emit("change");
                    break;
                case ActionTypes.RESET_PRODUCTS:
                    _products = [];
                    _bufferedProducts = [];
                    this.emit("change");
                    break;
                default:
                // do nothing
            }
        });
    }

    resetProducts() {
        _products = [];
        return _products;
    }

    getSort() {
        return _sort;
    }
    getAll() {
        return _products;
    }

    getAllCount() {
        return _products.length + _bufferedProducts.length;
    }

    getLoadingStatus() {
        return _loadingStatus;
    }

    getIsListFinished() {
        return _isListFinished;
    }
}

export default new ProductStore();
