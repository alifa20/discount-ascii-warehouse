import http from 'http';
import ServerActions from "./actions/ServerActions";
import request from 'superagent';
import NJStream from 'njstream';

let API = {
    fetchProducts(skip, sort) {
        ServerActions.loadingProducts({ status: true });
        let njstream = new NJStream();
        let body = '';
        let limit = 15;
        let sort_str = '';

        if (!!sort) {
            sort_str = `sort=${sort}&`;
        }

        if (!skip) {
            skip = 0;
        }

        if (skip == 0) {
            limit = 30;
        } else {
            ServerActions.loadBufferToProducts();
            ServerActions.loadingProducts({ status: false });
        }

        let url = `/api/products?${sort_str}limit=${limit}&skip=${skip}`;
        njstream.on('parsed', json_data => {
            ServerActions.receiveProducts({ sort, skip, limit, data: json_data });
        });

        console.info('GET: ', url);
        http.get(url, res => {
            res.on('data', chunk => {
                body += chunk;
            });
            res.on('end', (e, s) => {
                // all data has been downloaded
                ServerActions.loadingProducts({ status: false });
            }).pipe(njstream);
        });
    }
};

export default API;
